package com.agiletestingalliance;

import static org.junit.Assert.*;
import org.junit.Test;

public class MinMaxTest {

    @Test
    public void testReturnBiggerFirstBigger() throws Exception {
       	int result = new MinMax().returnBigger(5,6);
	assertEquals("MainPage",6,result);
    }

    @Test
    public void testReturnBiggerSecondBigger() throws Exception {
        int result = new MinMax().returnBigger(6,5);
        assertEquals("MainPage",6,result);
    }

	
    @Test
    public void testBarNotEmpty() throws Exception {
        String result = new MinMax().bar("Not Empty");
        assertTrue("MainPage", result.equals("Not Empty"));
    }





}
